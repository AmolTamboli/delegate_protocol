//
//  ViewController.swift
//  Delegate_&_Protocol
//
//  Created by Amol Tamboli on 27/06/20.
//  Copyright © 2020 Amol_Tamboli. All rights reserved.
//

import UIKit

class ViewController: UIViewController,dataPass {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func btnGoToForm(_ sender: Any) {
        let secondVC = self.storyboard?.instantiateViewController(identifier: "SecondViewController") as! SecondViewController
        secondVC.delegate = self
        self.navigationController?.pushViewController(secondVC, animated: true)
    }
    
    //    func dataPassing(name: String, address: String, city: String) {
    //        self.lblName.text = name
    //        self.lblCity.text = city
    //        self.lblAddress.text = address
    //    }
    
    func data(object: [String : String]) {
        self.lblName.text = object["name"]
        self.lblCity.text = object["city"]
        self.lblAddress.text = object["address"]
    }
    
}

