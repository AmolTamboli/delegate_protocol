//
//  SecondViewController.swift
//  Delegate_&_Protocol
//
//  Created by Amol Tamboli on 27/06/20.
//  Copyright © 2020 Amol_Tamboli. All rights reserved.
//

import UIKit
protocol dataPass {
    //func dataPassing(name:String, address:String, city:String)
    func data(object:[String:String])
}
class SecondViewController: UIViewController {
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    
    var delegate:dataPass!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func btnSave(_ sender: Any) {
        if txtName.text == "" {
            self.showAlert(title: "Enter Name", msg: "")
        } else if txtAddress.text == "" {
            self.showAlert(title: "Enter Address", msg: "")
        } else if txtCity.text == "" {
            self.showAlert(title: "Enter City", msg: "")
        } else{
          //  delegate.dataPassing(name: txtName.text!, address: txtAddress.text!, city: txtCity.text!)
            let dict:[String:String] = ["name":txtName.text!,"address":txtAddress.text!,"city":txtCity.text!]
            delegate.data(object: dict)
            self.navigationController!.popViewController(animated: true)
        }
    }    
}

extension UIViewController {
    func showAlert(title: String, msg: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}
